/**
 * Page for enhance (cut and filters)
 * @todo: #33! Add info about episode_bid (check auth too)
 *             movie_template.name, .duration_of_episodes,
 *             episode_template.name, story, conds, order_in_movie
 */
module.exports = {
  block: 'page-enhance',
  tag: 'body',
  content: [{
    elem: 'header',
    content: ['_site-header.bj.js']
  }, {
    elem: 'workspace',
    mix: {
      block: 'bsp-container'
    },
    content: ['_notif-row.bj.js', {
      elem: 'row',
      mix: {
        block: 'bsp-row'
      },
      content: [{
        elem: 'col-main',
        mix: {
          block: 'bsp-col',
          mods: {
            md: 8,
            sm: 12
          }
        },
        content: [{
          block: 'over-wrap',
          content: [{
            block: 'ratio16-9',
            content: [{
              elem: 'workspace',
              content: [{
                block: 'enh-loader',
                content: 'conversion...'
              }, {
                block: 'enh-player'
              }]
            }]
          }]
        }]
      }, {
        elem: 'col-side',
        mix: {
          block: 'bsp-col',
          mods: {
            md: 4,
            sm: 12
          }
        },
        content: [{
          elem: 'cell-side-1',
          mix: {
            block: 'over-wrap'
          }
        }, {
          elem: 'cell-side-2',
          mix: {
            block: 'over-wrap'
          }
        }]
      }]
    }]
  }, {
    elem: 'footer',
    content: ['_license-info.bj.js']
  }, '_popup-scope.bj.js', {
    block: 'jquery-script',
    tag: 'script',
    bem: false,
    attrs: {
      src: './libs/jquery.js'
    }
  }, {
    elem: 'videojs-script',
    tag: 'script',
    bem: false,
    attrs: {
      //        src: '//vjs.zencdn.net/4.9.1/video.js'
      src: './libs/video.js'
    }
  }, {
    block: 'page-script',
    tag: 'script',
    bem: false,
    attrs: {
      src: './js/enhance-bundle.js'
    }
  }]
};
